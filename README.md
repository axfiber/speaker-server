# speaker-server

#### 介绍
文字转语音服务，适用于windows、linux下文字转语音

#### 软件架构
软件架构说明
![输入图片说明](https://images.gitee.com/uploads/images/2021/0510/161528_7db833ad_2292452.png "屏幕截图.png")

#### 安装教程

1.  git clone
2.  maven导入依赖
3.  运行

#### 使用说明

1. windows下将lib下的dll文件复制到jdk的bin目录下，修改appliction.yml文件中的"system.runtime.platform"为1,,调用"/speaker/sendVoiceMsg?text=?"即可。
2.  liunx下(需要有语音输出设备):
    centos: "sudo yum install espeak"后，修改appliction.yml文件中的"system.runtime.platform"为0,,调用"/speaker/sendVoiceMsg?text=?"即可.
     ubuntu:  "sudo apt install espeak"后，修改appliction.yml文件中的"system.runtime.platform"为0,,调用"/speaker/sendVoiceMsg?text=?"即可.


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
