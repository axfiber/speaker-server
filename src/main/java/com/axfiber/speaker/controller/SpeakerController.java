package com.axfiber.speaker.controller;

import com.axfiber.speaker.service.SpeakerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * SpeakerController
 *
 * @author Zhan Xinjian
 * @date 2021/5/10
 */
@RequestMapping("/speaker")
@RestController
@Slf4j
public class SpeakerController {
    @Resource
    private SpeakerService speakerService;

    @RequestMapping("/sendVoiceMsg")
    public String  sendVoiceMsg(@RequestParam("text") String text) throws IOException {
        speakerService.convertTextToVoice(text);
        log.info("输入的语音信息为:{}",text);
        return "OK";
    }

}
