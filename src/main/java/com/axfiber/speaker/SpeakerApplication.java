package com.axfiber.speaker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Zhan Xinjian
 * @date 2021/5/10
 */
@SpringBootApplication
public class SpeakerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpeakerApplication.class, args);
    }

}
