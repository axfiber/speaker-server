package com.axfiber.speaker.service;

import java.io.IOException;

/**
 * @author Zhan Xinjian
 * @date 2021/5/10
 */
public interface SpeakerService {
    /**
     * 文字转语音输出
     *
     * @param text 需要转换的文字
     * @throws IOException IOException
     */
    void convertTextToVoice(String text) throws IOException;
}
