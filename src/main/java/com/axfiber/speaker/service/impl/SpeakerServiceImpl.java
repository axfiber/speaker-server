package com.axfiber.speaker.service.impl;

import com.axfiber.speaker.service.SpeakerService;
import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.Variant;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;

/**
 * SpeakerService实现类
 *
 * @author Zhan Xinjian
 * @date 2021/5/10
 */
@Service
public class SpeakerServiceImpl implements SpeakerService {
    @Value("${system.runtime.platform}")
    private Integer platform;

    @Override
    public void convertTextToVoice(String text) throws IOException {
        if (platform == 0) {
            linuxSpeaker(text);
        } else {
            textToSpeech(text);
        }
    }

    /**
     * 调用linux下文字转语音服务，需要yum install espeak
     *
     * @param text 文字
     * @throws IOException IOException
     */
    private void linuxSpeaker(String text) throws IOException {
        Runtime.getRuntime().exec("espeak -vzh \"" + text + "\"");
    }


    /**
     * 语音转文字并播放,需要将lib下的dll放到jdk安装目录
     *
     * @param text 文字
     */
    private static void textToSpeech(String text) {
        ActiveXComponent ax;
        try {
            ax = new ActiveXComponent("Sapi.SpVoice");

            // 运行时输出语音内容
            Dispatch spVoice = ax.getObject();
            // 音量 0-100
            ax.setProperty("Volume", new Variant(100));
            // 语音朗读速度 -10 到 +10
            ax.setProperty("Rate", new Variant(-2));
            // 执行朗读
            Dispatch.call(spVoice, "Speak", new Variant(text));
            // 下面是构建文件流把生成语音文件
            ax = new ActiveXComponent("Sapi.SpFileStream");
            Dispatch spFileStream = ax.getObject();

            ax = new ActiveXComponent("Sapi.SpAudioFormat");
            Dispatch spAudioFormat = ax.getObject();

            // 设置音频流格式
            Dispatch.put(spAudioFormat, "Type", new Variant(22));
            // 设置文件输出流格式
            Dispatch.putRef(spFileStream, "Format", spAudioFormat);
            // 调用输出 文件流打开方法，创建一个.wav文件
            String filePath = "./text.wav";
            Dispatch.call(spFileStream, "Open", new Variant(filePath), new Variant(3), new Variant(true));
            // 设置声音对象的音频输出流为输出文件对象
            Dispatch.putRef(spVoice, "AudioOutputStream", spFileStream);
            // 设置音量 0到100
            Dispatch.put(spVoice, "Volume", new Variant(100));
            // 设置朗读速度
            Dispatch.put(spVoice, "Rate", new Variant(-2));
            // 开始朗读
            Dispatch.call(spVoice, "Speak", new Variant(text));

            // 关闭输出文件
            Dispatch.call(spFileStream, "Close");
            Dispatch.putRef(spVoice, "AudioOutputStream", null);
            spAudioFormat.safeRelease();
            spFileStream.safeRelease();
            spVoice.safeRelease();
            ax.safeRelease();
            //之后删除掉wav文件
            new File(filePath).delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
